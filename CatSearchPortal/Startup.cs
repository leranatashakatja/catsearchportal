﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CatSearchPortal.Startup))]
namespace CatSearchPortal
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
