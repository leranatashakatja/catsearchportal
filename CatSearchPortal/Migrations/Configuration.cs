namespace CatSearchPortal.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using CatSearchPortal.Models;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;

    internal sealed class Configuration : DbMigrationsConfiguration<CatSearchPortal.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(CatSearchPortal.Models.ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
            var store = new RoleStore<IdentityRole>(context);
            var manager = new RoleManager<IdentityRole>(store);
            if (!context.Roles.Any(r => r.Name == "Administrator"))
            {                
                var role = new IdentityRole { Name = "Administrator" };

                manager.Create(role);
            }
            if (!context.Roles.Any(r => r.Name == "Cattery"))
            {
                var role = new IdentityRole { Name = "Cattery" };

                manager.Create(role);
            }
            if (!context.Roles.Any(r => r.Name == "User"))
            {
                var role = new IdentityRole { Name = "User" };

                manager.Create(role);
            }

            var userstore = new UserStore<ApplicationUser>(context);
            var usersmanager = new UserManager<ApplicationUser>(userstore);
            var PasswordHash = new PasswordHasher();

            if (!context.Users.Any(u => u.UserName == "Administrator"))
            {                
                var user = new ApplicationUser { 
                    UserName = "Administrator",
                    Email = "catsearchportal@gmail.com",
                    EmailConfirmed = true,
                    PasswordHash = PasswordHash.HashPassword("Ilovec#123")
                };

                usersmanager.Create(user);
                usersmanager.AddToRole(user.Id, "Administrator");
            }
            if (!context.Users.Any(u => u.UserName == "user1@contoso.com"))
            {
                var user = new ApplicationUser
                {
                    UserName = "user1@contoso.com",
                    PasswordHash = PasswordHash.HashPassword("P_assw0rd1")
                };

                usersmanager.Create(user);
                usersmanager.AddToRole(user.Id, "User");
            }
            if (!context.Users.Any(u => u.UserName == "user2@contoso.com"))
            {
                var user = new ApplicationUser
                {
                    UserName = "user2@contoso.com",
                    PasswordHash = PasswordHash.HashPassword("P_assw0rd2")
                };

                usersmanager.Create(user);
                usersmanager.AddToRole(user.Id, "User");
            }
        }
    }
}
